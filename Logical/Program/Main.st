
PROGRAM _INIT
	(* Insert code here *)
  SetTemperature := 35;
  CurrentTemperature := 36;
  TemperatureChangeValue := 1;
	END_PROGRAM

PROGRAM _CYCLIC
  DeltaMinus := SetTemperature - 5;
  DeltaPlus := SetTemperature + 5;

  CurrentTemperature := CurrentTemperature + TemperatureChangeValue;
  IF TemperatureChangeValue > 0 THEN
    IF CurrentTemperature >= DeltaPlus THEN
      TemperatureChangeValue := -1;
    END_IF
  ELSE
    IF CurrentTemperature <= DeltaMinus THEN
      TemperatureChangeValue := +1;
    END_IF
  END_IF
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM